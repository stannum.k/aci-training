tenant_name        = "kamil"
tenant_description = "this tenant was created by kuran"
vmm_domain         = "uni/vmmp-VMware/dom-UCS-01_PROD_CIT"

networks = { 
  net1 = { 
    bd = "vlan_10",
    ip = "10.10.10.1/24",
    anp = "anp",
    epg = "vlan_10"
  },
  net2 = {
    bd = "vlan_20",
    ip = "10.20.20.1/24",
    anp = "anp",
    epg = "vlan_20"
  }
}

external_networks = { 
  public = {
    l3out = "public_l3out",
    l3_domain = "uni/l3dom-FORTINET_OUT",
    border_leaf_profile_name = "public_l3out_nodeProfle",
    border_leaf_interface_profile_name = "public_l3out_intProfle",
    external_epg_name = "public_l3out",
    external_epg_prefix = ["0.0.0.0/1","128.0.0.0/1"]
  }, 
  private = {
    l3out = "private_l3out",
    l3_domain = "uni/l3dom-FORTINET_OUT",
    border_leaf_profile_name = "private_l3out_nodeProfle",
    border_leaf_interface_profile_name = "private_l3out_intProfle",
    external_epg_name = "private_l3out", external_epg_prefix = ["10.0.0.0/8"]
  }
}
