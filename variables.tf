variable "tenant_name" {
}
variable "tenant_description" {
}
variable "aci_user" {
  type      = string
  sensitive = true
}
variable "aci_pass" {
  type      = string
  sensitive = true
}
variable "aci_url" {
  type      = string
  sensitive = true
}
variable "vmm_domain" {
}
variable "networks" {
  description = "Create network, that contains BD, subnet, EPG"
  type        = map(any)
}
variable "anp" {
  default = ""
}

variable "external_networks" {
  description = "Create list of external networks - public and private"
  type = map(object( {
    l3out = string
    l3_domain = string
    border_leaf_profile_name = string
    border_leaf_interface_profile_name = string
    external_epg_name = string
    external_epg_prefix = any
  } ) )
}
