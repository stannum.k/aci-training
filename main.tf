terraform {
  required_providers {
    aci = {
      source = "ciscodevnet/aci"
    }
  }
  required_version = ">= 0.13"
}

provider "aci" {
  username = var.aci_user
  password = var.aci_pass
  url      = var.aci_url
  insecure = true
}

resource "aci_tenant" "tenant" {
  description = var.tenant_description
  name        = var.tenant_name
  annotation  = "orchestrator:terraform"
}

resource "aci_vrf" "inside" {
  tenant_dn   = aci_tenant.tenant.id
  name        = "inside"
  pc_enf_pref = "enforced"
}

resource "aci_vrf" "outside" {
  tenant_dn = aci_tenant.tenant.id
  name      = "outside"
}

resource "aci_bridge_domain" "bd1" {
  tenant_dn          = aci_tenant.tenant.id
  relation_fv_rs_ctx = aci_vrf.outside.id
  name               = "vlan_10"
}

resource "aci_bridge_domain" "bd2" {
  tenant_dn          = aci_tenant.tenant.id
  relation_fv_rs_ctx = aci_vrf.outside.id
  name               = "vlan_20"
}

resource "aci_subnet" "subnet1" {
  parent_dn = aci_bridge_domain.bd1.id
  ip        = "10.10.10.1/24"
  scope     = ["public"]
}

resource "aci_subnet" "subnet2" {
  parent_dn = aci_bridge_domain.bd2.id
  ip        = "10.20.20.1/24"
  scope     = ["public"]
}


# Homework #1
resource "aci_application_profile" "anp" {
  tenant_dn   = aci_tenant.tenant.id
  name        = "anp"
  description = "This app profile is created by terraform from mkhavank-pc"
}

resource "aci_application_epg" "epg1" {
  application_profile_dn = aci_application_profile.anp.id
  relation_fv_rs_bd      = aci_bridge_domain.bd1.id
  name                   = "vlan_10"
  pref_gr_memb           = "include"
}

resource "aci_application_epg" "epg2" {
  application_profile_dn = aci_application_profile.anp.id
  relation_fv_rs_bd      = aci_bridge_domain.bd2.id
  name                   = "vlan_20"
  pref_gr_memb           = "include"
}

# Homework #2
resource "aci_epg_to_domain" "epg1_to_vmm1" {
  application_epg_dn = aci_application_epg.epg1.id
  tdn                = var.vmm_domain
  instr_imedcy       = "immediate"
  res_imedcy         = "immediate"
}

resource "aci_epg_to_domain" "epg2_to_vmm1" {
  application_epg_dn = aci_application_epg.epg2.id
  tdn                = var.vmm_domain
  instr_imedcy       = "immediate"
  res_imedcy         = "immediate"
}

resource "aci_vrf" "vrf1" {
  tenant_dn   = aci_tenant.tenant.id
  name        = "inside"
  pc_enf_pref = "enforced"
}

resource "aci_bridge_domain" "bd" {
  for_each           = var.networks
  tenant_dn          = aci_tenant.tenant.id
  relation_fv_rs_ctx = aci_vrf.vrf1.id
  name               = each.value.bd
}

resource "aci_l3_outside" "l3" {
  for_each                     = var.external_networks
  tenant_dn                    = aci_tenant.tenant.id
  name                         = each.value.l3out
  relation_l3ext_rs_ectx       = aci_vrf.vrf1.id
  relation_l3ext_rs_l3_dom_att = each.value.l3_domain
}

resource "aci_logical_node_profile" "border_leaf_profile" {
  for_each      = var.external_networks
  l3_outside_dn = aci_l3_outside.l3[each.key].id
  name          = each.value.border_leaf_profile_name
}

resource "aci_logical_interface_profile" "border_leaf_logical_int_profile" {
  for_each                = var.external_networks
  logical_node_profile_dn = aci_logical_node_profile.border_leaf_profile[each.key].id
  name                    = each.value.border_leaf_interface_profile_name
}

resource "aci_external_network_instance_profile" "external_epg" {
  for_each      = var.external_networks
  l3_outside_dn = aci_l3_outside.l3[each.key].id
  name          = each.value.external_epg_name
  pref_gr_memb  = "include"
}

locals {
  /* Определение "плоской" структуры в которой присутствуют идентификторы внешних сетей и префиксы, которые должны присутствовать в конфигурации в виде атрибута "External Subnets for the External EPG" На выходе получается list следующего вида [ { "external_key" = "private" "prefix_key" = "10.0.0.0/8" }, { "external_key" = "public" "prefix_key" = "0.0.0.0/1" }, { "external_key" = "public" "prefix_key" = "128.0.0.0/1" }, ] Этот лист будет использоваться при создании ресурса aci_l3_ext_subnet */
  external_epg_by_prefix_Flat = flatten ([
    for external_key, external_networks in var.external_networks : [
      for prefix_key, external_epg_prefix in external_networks.external_epg_prefix : {
        external_key = external_key
        prefix_key = external_networks.external_epg_prefix[prefix_key] 
      }
    ]
  ])
}

resource "aci_l3_ext_subnet" "external_subnet" {
  for_each = {
    for prefix in local.external_epg_by_prefix_Flat : "${prefix.external_key}.${prefix.prefix_key}" => prefix
  }
  external_network_instance_profile_dn = aci_external_network_instance_profile.external_epg[each.value.external_key].id
  ip = each.value.prefix_key
}

resource "aci_rest" "bgp_on_l3out" {
  depends_on = [aci_l3_outside.l3]
  for_each = var.external_networks
  path = join("",["/api/node/mo/uni/tn-",var.tenant_name,"/out-",each.value.l3out,"/bgpExtP.json"])
  class_name = "bgpExtP"
  content = { "dn" = join("",["uni/tn-",var.tenant_name,"/out-",each.value.l3out,"/bgpExtP"]) }
}
